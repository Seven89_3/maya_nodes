#pragma once

#include <maya/MPxTransform.h>
#include <maya/MPxTransformationMatrix.h>
#include <maya/MPxNode.h>

#include <maya/MFnNumericAttribute.h>

static const int NODE_STATIC_HRC_ID = 0x8101B;
static const char * NODE_STATIC_HRC_NAME= "static_hrc";

static const int NODE_STATIC_MATRIX_ID = 0x8101C;

//			STATIC HRC Node			//

class StaticMatrix : public MPxTransformationMatrix {
public:
	static MTypeId id;

	StaticMatrix();

	static void* creator();

	virtual MMatrix asMatrix() const;
	virtual MMatrix asMatrix(double percent) const;

protected:
	typedef MPxTransformationMatrix ParentClass;

};

//			STATIC HRC Node			//

class StaticHrc : public MPxTransform {
public:
	static MTypeId id;
	static MString name;
	
	StaticHrc();
	virtual ~StaticHrc();

	static void * creator();
	static MStatus initialize();
	void postConstructor();

	MPxTransformationMatrix* createTransformationMatrix();

	virtual MStatus compute(MPlug& plug, MDataBlock& datablock);

protected:
	typedef MPxTransform ParentClass;

};