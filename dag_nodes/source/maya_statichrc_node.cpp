#include "../header/maya_dag_nodes.h"

//		STATIC HRC Matrix		//

MTypeId StaticMatrix::id = NODE_STATIC_MATRIX_ID;

StaticMatrix::StaticMatrix(){}

void* StaticMatrix::creator(){
	return new StaticMatrix();
}

MMatrix StaticMatrix::asMatrix() const{
	return MMatrix::identity;
}

MMatrix StaticMatrix::asMatrix(double percent) const {
	return MMatrix::identity;
}

//		STATIC HRC Node		//	
MTypeId StaticHrc::id = NODE_STATIC_HRC_ID;
MString StaticHrc::name = NODE_STATIC_HRC_NAME;

StaticHrc::StaticHrc() : ParentClass() {}

StaticHrc::~StaticHrc() {}

void * StaticHrc::creator() {
	return new StaticHrc();
}

MStatus StaticHrc::initialize() {
	return MStatus::kSuccess;
}

void StaticHrc::postConstructor() {
	MPxTransform::postConstructor();
}

MPxTransformationMatrix* StaticHrc::createTransformationMatrix() {
	return new StaticMatrix();
}

MStatus StaticHrc::compute(MPlug& plug, MDataBlock& datablock) {
	return MStatus::kSuccess;
}