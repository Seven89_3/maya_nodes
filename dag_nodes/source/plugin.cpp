#include "../header/maya_dag_nodes.h"
#include <maya/MFnPlugin.h>

MStatus initializePlugin(MObject pluginMob) {
	MStatus status;
	MFnPlugin fn;

	status = fn.setName("maya_nodes");

	status = fn.registerTransform(StaticHrc::name, 
								 StaticHrc::id,
								 &StaticHrc::creator,
								 &StaticHrc::initialize,
								 (MCreateXformMatrixFunction)&StaticMatrix::creator,
								 StaticMatrix::id, nullptr);
	return MS::kSuccess;
}

MStatus uninitializePlugin(MObject pluginMob) {
	
	MFnPlugin fn;
	fn.deregisterNode(StaticHrc::id);

	return MS::kSuccess;
}